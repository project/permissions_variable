<?php
/**
 * @file
 * Documentation and examples for Permissions Variable module hooks.
 */

/**
 * Allows permissions to be altered prior to import or display to users.
 *
 * @param array $permissions
 *   An array of [$permission_name => $roles] pairs, whereby $roles are the
 *   roles that have been granted the permission with name $permission_name.
 */
function hook_permissions_variable_alter(array &$permissions) {
  foreach ($permissions as $name => $roles) {
    $matches = array();
    if (preg_match("/((edit|delete) terms in )([^\s]+)$/", trim($name), $matches)) {
      $prefix = $matches[1];
      $machine_name = $matches[3];
      if ($vocab = taxonomy_vocabulary_machine_name_load($machine_name)) {
        $new_name = $prefix . $vocab->vid;
        // Unset the old permission.
        unset($permissions[$name]);
        // Replace with the new permission with machine_name converted to vid.
        $permissions[$new_name] = $roles;
      }
    }
  }
}

/**
 * Allows all permissions to be altered prior to export.
 *
 * @param array $permission
 *   A single permission in the following structure:
 *   $permission = [
 *     'module' => string,
 *     'name' => string,
 *     'title' => string,
 *     'roles' => [string1, ... stringN],
 *   ].
 */
function hook_permissions_variable_export_alter(array &$permission) {
  if ($permission['module'] != 'taxonomy') {
    return;
  }

  $permission['name'] = preg_replace_callback(
    "/((edit|delete) terms in )(\d+)$/",
    function($matches) {
      $prefix = $matches[1];
      $vid = $matches[3];
      if ($vocab = taxonomy_vocabulary_load($vid)) {
        return $prefix . $vocab->machine_name;
      }
      return $prefix . $vid;
    },
    trim($permission['name'])
  );
}

/**
 * Allows a specific module's permissions to be altered prior to export.
 *
 * @param array $permission
 *   A single permission in the following structure:
 *   $permission = [
 *     'module' => string,
 *     'name' => string,
 *     'title' => string,
 *     'roles' => [string1, ... stringN],
 *   ].
 */
function hook_permissions_variable_export_MODULE_NAME_alter(array &$permission) {
  $permission['name'] = preg_replace_callback(
    "/((edit|delete) terms in )(\d+)$/",
    function($matches) {
      $prefix = $matches[1];
      $vid = $matches[3];
      if ($vocab = taxonomy_vocabulary_load($vid)) {
        return $prefix . $vocab->machine_name;
      }
      return $prefix . $vid;
    },
    trim($permission['name'])
  );
}
