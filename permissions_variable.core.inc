<?php
/**
 * @file
 * Hooks implemented on behalf of core modules.
 */

/**
 * Implements hook_permissions_variable_alter().
 *
 * On behalf of the core taxonomy module.
 */
function taxonomy_permissions_variable_alter(&$permissions) {
  foreach ($permissions as $name => $roles) {
    $matches = array();
    if (preg_match("/((edit|delete) terms in )([^\s]+)$/", trim($name), $matches)) {
      $prefix = $matches[1];
      $machine_name = $matches[3];
      if ($vocab = taxonomy_vocabulary_machine_name_load($machine_name)) {
        $new_name = $prefix . $vocab->vid;
        // Unset the old permission.
        unset($permissions[$name]);
        // Replace with the new permission with machine_name converted to vid.
        $permissions[$new_name] = $roles;
      }
    }
  }
}

/**
 * Implements hook_permissions_variable_export_taxonomy_alter().
 *
 * On behalf of the core taxonomy module.
 */
function taxonomy_permissions_variable_export_taxonomy_alter(&$permission) {
  $permission['name'] = preg_replace_callback(
    "/((edit|delete) terms in )(\d+)$/",
    function($matches) {
      $prefix = $matches[1];
      $vid = $matches[3];
      if ($vocab = taxonomy_vocabulary_load($vid)) {
        return $prefix . $vocab->machine_name;
      }
      return $prefix . $vid;
    },
    trim($permission['name'])
  );
}
